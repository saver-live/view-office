# view-office
（收费版本说明）
[https://blog.csdn.net/weixin_39484550/article/details/105858607](https://blog.csdn.net/weixin_39484550/article/details/105858607)

#### 收费版本演示视频
[https://v.youku.com/v_show/id_XNDY0NzkwNjg0NA==.html](https://v.youku.com/v_show/id_XNDY0NzkwNjg0NA==.html)

#### 介绍
使用jacob，poi来实现在线访问docx，doc,xls(支持但显示效果不好),xlsx,ppt，pptx的文件
（如果有需要可以提供又收费服务，2000元永久使用，支持在线编辑word，excel，ppt等office格式文件，qq：1005471232）

#### 软件原理
jacob只支持window服务器,使用jacob对word，ppt文件转换为pdf文件来在线查看，使用poi读取数据，再显示再html


#### 安装教程

1. 自行安装office组件
2. 下载该项目
3. 配置application.yml的文件缓存路径,这个路径必须存在
   app:
     filePath: D:\web\
3. 使用idea编译该spring boot项目
4. 执行 java -jar demo.jar

#### 使用说明

1. 调用接口：http://127.0.0.1/perview/office?src=目标文件请求路径

2. word测试请求：http://127.0.0.1/sample/preview/word
![输入图片说明](https://images.gitee.com/uploads/images/2019/0419/173459_cf38794c_1438804.jpeg "SharedScreenshot2.jpg")
3. excel测试请求：http://127.0.0.1/sample/preview/excel
![输入图片说明](https://images.gitee.com/uploads/images/2019/0419/173516_228c806f_1438804.jpeg "SharedScreenshot.jpg")
4. ppt测试请求：http://127.0.0.1/sample/preview/ppt

### 捐赠
![输入图片说明](https://images.gitee.com/uploads/images/2019/0422/085855_688bc575_1438804.jpeg "IMG_0114.JPG")
